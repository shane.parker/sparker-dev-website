import React from "react";
import Header from "./Header";
import Section from "./Section";
import Scroller from "./Scroller";
import Featured from "./Featured";
import Contact from "./Contact";
import Footer from "./Footer";

import "./App.css";

const items = [
  "websites",
  "web applications",
  "mobile applications",
  "database solutions",
  "conversions improve",
  "AWS solutions",
  "micro-services",
  "API integrations",
  "single-page apps",
  "eCommerce solutions",
  "payment integrations",
  "android apps",
  "javascript solutions",
  "software work",
  "iOS apps",
  "java solutions",
  "shopify websites",
  "multi-media apps",
  "deployment solutions",
  "mobile games",
  "responsive websites",
  "bugs go away",
];

const fonts = [
  "40px Nunito",
  "24px PT Mono"
];

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };
  }

  async loadFonts() {
    return document.fonts ? Promise.all(fonts.map( f => document.fonts.load(f) ))
      : true;
  }

  componentDidMount() {
    window.onload = async () => {
      await this.loadFonts();
      this.setState({visible: true});
    };
  }

  render() {
    const {visible} = this.state;
    if ( !visible ) {
      return (<div className="app loading"></div>);
    }

    return (
      <div className="app">
        <Header />

        <div className="body">
          <Section className="promo gray below-header scroller-section">
            <span><b>sparker</b> makes your<Scroller items={items} />.</span>
          </Section>

          <div className="grid columns three featured-section">
            <div className="cell first">
              <Featured text="Cloud development and deployment" />
            </div>
            <div className="cell second">
              <Featured text="Custom websites and mobile apps" />
            </div>
            <div className="cell third">
              <Featured text="Software consultation and support" />
            </div>
          </div>

          <Section className="promo gray h1">
            <span>recent projects</span>
          </Section>

          <Section className="light">
            <span>PORTFOLIO SECTION HERE</span>
          </Section>

          <Section className="promo gray h2">
            <span>say hello!</span>
          </Section>

          <Section className="light">
            <Contact />
          </Section>
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
