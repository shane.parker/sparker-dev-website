import React from "react";

import qrImg from "./qr.png";
import "./Contact.css";

class Field extends React.Component {
  render() {
    const {
      name,
      title,
      placeholder = "",
      big = false
    } = this.props;

    return (
      <div className="field">
        <label for={name}>{title || name}</label>
        {(big === false) ? (<input name={name} placeholder={placeholder}></input>) :
          (<textarea name={name} placeholder={placeholder}></textarea>)}
      </div>
    );
  }
}

class Contact extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="contact-header">Fill out the form below to get in touch, or scan the QR code for contact information.</div>

        <div className="contact">
          <form name="contact">
            <Field name="name" placeholder="name..." />
            <Field name="company" placeholder="Widgets Inc." />
            <Field name="email" placeholder="you@email.com" />
            <Field name="message" placeholder="message..." big={true} />

            <button>Send</button>
          </form>

          <div className="qr-container">
            <img src={qrImg} alt="QR Code" />
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
