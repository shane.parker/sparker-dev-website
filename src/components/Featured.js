import React from "react";

import "./Featured.css";

const Featured = (props) => (
  <div className="feature">
    <span className="text">{props.text}</span>
  </div>
);

export default Featured;
