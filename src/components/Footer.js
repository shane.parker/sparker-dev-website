import React from "react";

import bunnyImg from "./bunny.svg";
import "./Footer.css";

const Footer = () => (
  <footer className="footer">
    <div className="label">copyright 2019 Shane Parker</div>
    <img className="bunny" src={bunnyImg} />
  </footer>
);

export default Footer;
