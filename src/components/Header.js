import React from "react";
import * as scrollToElement from "scroll-to-element";

import Logo from "./Logo";
import arrow from "./arrow.svg";
import "./Header.css";

const scrollDown = () => {
  scrollToElement(".section", {
    offset: 0,
    duration: 450
  });
};

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      transition: 1.0
    };

    this.scrollHandler = this.scrollHandler.bind(this);
  }

  componentDidMount() {
    //window.addEventListener("scroll", this.scrollHandler);
    //window.addEventListener("resize", this.scrollHandler);

    const rect = this.ref.getBoundingClientRect();
    this.h = rect.height;
  }

  componentWillUnmount() {
    //window.removeEventListener("scroll", this.scrollHandler);
    //window.removeEventListener("resize", this.scrollHandler);
  }

  scrollHandler() {
    const {scale = 1.0} = this.props;
    const interp = Math.max(0.0, 1.0 - ((window.scrollY / this.h) * scale));

    this.setState({transition: Math.min(1.0, interp)});
  }

  render() {
    const {transition} = this.state;

    return (<header ref={(r) => this.ref = r} className="header">
      <div style={{opacity: transition, transform: `scale(${transition})`}} className="container">
        <Logo />
        <div className="lower fade-in" onClick={scrollDown}>
          <div className="label">software services</div>
          <img src={arrow} className="arrow" alt="arrow" />
        </div>
      </div>
    </header>);
  }
}

export default Header;
