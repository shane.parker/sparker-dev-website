import React from "react";

import logoSVG from "./logo.svg";
import logoBG from "./logo-bg.png";

import "./Logo.css";

const logo = (props) => {
  return (
    <div style={props.style} className="logo-container">
      <div style={{backgroundImage: `url(${logoBG})`}} className="logo-bg" alt="logo background"></div>
      <img src={logoSVG} className="logo" alt="logo" />
    </div>
  );
}

export default logo;
