import React from "react";
import "./Scroller.css";

class Scroller extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      textIn: "",
      textOut: "",
      index: 0
    };
  }

  componentDidMount() {
    const {interval = 2000} = this.props;
    this.interval = setInterval(this.pluckItem.bind(this), interval);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  pluckItem() {
    const {textIn: textOut, index} = this.state;
    const {items = []} = this.props;

    const textIn = items[index % items.length] || "";
    this.setState({textOut, textIn, index: index+1});
  }

  render() {
    const {className = ""} = this.props;
    const {textIn, textOut} = this.state;
    const time = Date.now();

    return (
      <span className={`scroller ${className}`}>
        <span key={`t1_${time}`} className="text fade-in">{textIn}</span>
        <span key={`t2_${time}`} className="text fade-out">{textOut}</span>
      </span>
    );
  }
}

export default Scroller;
